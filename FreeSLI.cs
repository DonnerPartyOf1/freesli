using System;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Collections.Generic;
using Microsoft.Win32;


class FreeSLI
{

    static byte[] hexmarker = {0x40, 0xc0, 0xef, 0x04, 0x40, 0xf6, 0xd6, 0x40, 0xf6, 0xd7, 0x40, 0x80, 0xe6, 0x01, 0x40, 0x80, 0xe7, 0x01};

    //MOV dword ptr[RBX + 0x24],0x00000000
    static byte[] replacementInstruction = {0xc7, 0x43, 0x24, 0x00, 0x00, 0x00, 0x00};

    static string driverPath = null;
    static string driverFileName = "nvlddmkm.sys";

    static string[] distribToolNames = {"makecert.exe", "CertMgr.exe", "ChecksumFix.exe", "signtool.exe"};
    static string[] builtinToolNames = {"certutil.exe", "takeown.exe", "icacls.exe"};
    static string[] bootUtilNames = {"EfiDSEFix.exe", "devcon.exe"};

    const int SM_CLEANBOOT = 67;

    [DllImport("user32.dll")]
    static extern int GetSystemMetrics(int smIndex);

    //Use with no arguments for scan, patch, & install

    //$FILEPATH
    //      patches the given file instead of scanning installed driver repository

    //--reset or -R
    //      disables DSE, restarts all display adapters, re-enables DSE

    //--msi or -M
    //      Re-enable Message-Signaled Interrupts on the GPUs.
    static void Main(string[] args) {
        using (WindowsIdentity identity = WindowsIdentity.GetCurrent())
        {
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            if (!principal.IsInRole(WindowsBuiltInRole.Administrator)) {
                Console.WriteLine("Please run FreeSLI as an Administrator.");
                return;
            }
        }

        if (args.Length > 0) {
			if (args[0] == "--reset" || args[0] == "-R") {
				Console.WriteLine("Executing DSE disable and device reset, then closing.");
				dseBypassAndDevReset();
				return;
			} else if (args[0] == "--msi" || args[0] == "-M") {
                Console.WriteLine("Enabling MSI on Nvidia GPUs.");
                msiEnable();
                return;
            }
            driverPath = args[0];
        } else {
            driverPath = searchFSForDriverTarget();
        }

        if (driverPath == null) {
            Console.WriteLine("Unable to locate any installed driver files. Exiting.");
            return;
        }

        Console.WriteLine("Patching file...");
        if (patchFile(driverPath) == 0) {
            Console.WriteLine("Patched file successfully.");
        } else {
            Console.WriteLine("Problem with patching. Exiting.");
        }
        if (GetSystemMetrics(SM_CLEANBOOT) == 2) {
            Console.WriteLine("Would you like to install the patched driver now? (Y/N)");
            ConsoleKey keypress;
            do {
                keypress = Console.ReadKey().Key;
                if (keypress == ConsoleKey.Y) {
                    if (installDriver() == 0) {
                        Console.WriteLine("SLI-enabled driver installed successfully. Would you like to enable Message Signaled Interrupts? (Y/N)");
						do {
							keypress = Console.ReadKey().Key;
							if (keypress == ConsoleKey.Y) {
								msiEnable();
							} else if (keypress == ConsoleKey.Escape) {
								break;
							}
						}
						while (keypress != ConsoleKey.Y && keypress != ConsoleKey.N );
						Console.WriteLine("Please reboot with some form of test signing enabled.");
                    }
                    else {
                        Console.WriteLine("An error occurred during install. A more detailed message should have been logged.");
                    }
                } else if (keypress == ConsoleKey.Escape) {
                    break;
                }
            }
            while (keypress != ConsoleKey.Y && keypress != ConsoleKey.N );
        } else {
            Console.WriteLine("To install the driver, please restart in Safe Mode with Networking.");
        }
    }

    static string searchFSForDriverTarget() {
        //find latest installed driver
        string[] nvDriverFiles = Directory.GetFiles(Environment.GetEnvironmentVariable("SystemRoot") + 
													@"\System32\DriverStore\FileRepository\", 
													driverFileName, SearchOption.AllDirectories);
        var lastModified = new DateTime();
        string latestDriver = null;
        foreach (string driverFile in nvDriverFiles) {
            if (File.GetLastWriteTime(driverFile) > lastModified) {
                lastModified = File.GetLastWriteTime(driverFile);
                latestDriver = driverFile;
            }
        }
        if (nvDriverFiles.Length > 1) {
            Console.WriteLine("Found multiple Nvidia driver files.\nAssuming the latest install as target. If this is not the right driver, please use DDU and reinstall.");
        }
        return latestDriver;
    }

    static int patchFile(string filepath) {
        try {
            File.Copy(filepath, driverFileName, true);
        }
        catch (Exception e){
            Console.WriteLine("Unable to copy installed driver to working path. Permissions issue?" + e);
            return 1;
        }
        byte[] fileAsBytes = File.ReadAllBytes(filepath);

        int foundSeqBytes = 0;

        //program counter
        int pc = 0;

        foreach (byte currentByte in fileAsBytes) {
            if (currentByte == hexmarker[foundSeqBytes]) {
                foundSeqBytes++;
            } else if (currentByte == hexmarker[0]) {
                foundSeqBytes = 1;
            } else {
                foundSeqBytes = 0;
            }
            pc++;
            if (foundSeqBytes == hexmarker.Length) {
                break;
            }
        }

        if (foundSeqBytes != hexmarker.Length) {
            //never found the marker. return positive status indicator
            File.Delete(driverFileName);
            return 2;
        }

        //increment the program counter 7 places, 5 for the function call 
        //and 2 for the //TEST AL, AL// that does nothing since the result is never checked
        pc += 7;

        using (FileStream stream = File.OpenWrite(driverFileName)) {
            stream.Seek(pc, SeekOrigin.Begin);
            stream.Write(replacementInstruction, 0, replacementInstruction.Length);
            return 0;
        }
    }

    static int installDriver() {
        string[] toolFiles = Directory.GetFiles(Directory.GetCurrentDirectory(), distribToolNames[0], SearchOption.AllDirectories);
        if (toolFiles.Length < 1) {
            Console.WriteLine("Unable to find makecert.exe, cannot install");
            return 1;
        }
        string makecertExe = toolFiles[0];

        toolFiles = Directory.GetFiles(Directory.GetCurrentDirectory(), distribToolNames[1], SearchOption.AllDirectories);
        if (toolFiles.Length < 1) {
            Console.WriteLine("Unable to find CertMgr.exe, cannot install");
            return 1;
        }
        string certMgrExe = toolFiles[0];

        toolFiles = Directory.GetFiles(Directory.GetCurrentDirectory(), distribToolNames[2], SearchOption.AllDirectories);
        if (toolFiles.Length < 1) {
            Console.WriteLine("Unable to find ChecksumFix.exe, cannot install");
            return 1;
        }
        string checksumFixExe = toolFiles[0];

        toolFiles = Directory.GetFiles(Directory.GetCurrentDirectory(), distribToolNames[3], SearchOption.AllDirectories);
        if (toolFiles.Length < 1) {
            Console.WriteLine("Unable to find signtool.exe, cannot install");
            return 1;
        }
        string signtoolExe = toolFiles[0];

        toolFiles = Directory.GetFiles(Environment.GetEnvironmentVariable("SystemRoot") + @"\System32", builtinToolNames[0]);
        if (toolFiles.Length < 1) {
            Console.WriteLine("Unable to find certutil.exe, cannot install");
            return 1;
        }
        string certutilExe = toolFiles[0];

        toolFiles = Directory.GetFiles(Environment.GetEnvironmentVariable("SystemRoot") + @"\System32", builtinToolNames[1]);
        if (toolFiles.Length < 1) {
            Console.WriteLine("Unable to find takeown.exe, cannot install");
            return 1;
        }
        string takeownExe = toolFiles[0];

        toolFiles = Directory.GetFiles(Environment.GetEnvironmentVariable("SystemRoot") + @"\System32", builtinToolNames[2]);
        if (toolFiles.Length < 1) {
            Console.WriteLine("Unable to find icacls.exe, cannot install");
            return 1;
        }
        string icaclsExe = toolFiles[0];

        System.Diagnostics.Process externalProc = new System.Diagnostics.Process();
        externalProc.StartInfo.FileName = certutilExe;
        externalProc.StartInfo.Arguments = "-store root FreeSLI";
        externalProc.Start();
        externalProc.WaitForExit();

        if (externalProc.ExitCode != 0) {
            //cert hasn't been installed yet, so do it now
            externalProc.StartInfo.FileName = makecertExe;
            externalProc.StartInfo.Arguments = @"-r -pe -ss ""FreeSLI"" -n ""CN=FreeSLI"" " + Environment.GetEnvironmentVariable("SystemRoot") + @"\FreeSLI.cer";
            externalProc.Start();
            externalProc.WaitForExit();

            externalProc.StartInfo.FileName = certMgrExe;
            externalProc.StartInfo.Arguments = "/add " + Environment.GetEnvironmentVariable("SystemRoot") + @"\FreeSLI.cer " + "/s /r localMachine root";
            externalProc.Start();
            externalProc.WaitForExit();
        }

        externalProc.StartInfo.FileName = checksumFixExe;
        externalProc.StartInfo.Arguments = driverFileName;
        externalProc.Start();
        externalProc.WaitForExit();

        externalProc.StartInfo.FileName = signtoolExe;
        externalProc.StartInfo.Arguments = "sign /v /s FreeSLI /n FreeSLI /t http://timestamp.verisign.com/scripts/timstamp.dll nvlddmkm.sys";
        externalProc.Start();
        externalProc.WaitForExit();


        if (!driverPath.Contains(@"\System32\DriverStore\FileRepository\")) {
            //user passed in a driver from somewhere other than a valid driver path. Scan for latest installed driver.
            driverPath = searchFSForDriverTarget();
            if (driverPath == null) {
                Console.WriteLine("Unable to determine a proper driver install path. Reinstall your driver.");
                return 2;
            }
        }

        externalProc.StartInfo.FileName = takeownExe;
        externalProc.StartInfo.Arguments = "/f " + driverPath + " /a";
        externalProc.Start();
        externalProc.WaitForExit();

        externalProc.StartInfo.FileName = icaclsExe;
        externalProc.StartInfo.Arguments = driverPath + " /grant " + Environment.GetEnvironmentVariable("USERNAME") + ":f";
        externalProc.Start();
        externalProc.WaitForExit();

        try {
            File.Copy(driverFileName, driverPath, true);
        }
        catch (Exception e){
            Console.WriteLine("Unable to copy driver into install path. Permissions issue?" + e);
            return 1;
        }

        return 0;        
    }

    static void dseBypassAndDevReset() {
        string[] resetUtils = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, bootUtilNames[0], SearchOption.AllDirectories);
        if (resetUtils.Length < 1) {
            Console.WriteLine("Unable to find EfiDSEFix.exe, cannot disable DSE");
            return;
        }
        string efidsefixExe = resetUtils[0];

        resetUtils = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, bootUtilNames[1], SearchOption.AllDirectories);
        if (resetUtils.Length < 1) {
            Console.WriteLine("Unable to find devcon.exe, cannot automatically re-enable devices");
            return;
        }
        string devconExe = resetUtils[0];

        System.Diagnostics.Process externalProc = new System.Diagnostics.Process();
        externalProc.StartInfo.FileName = efidsefixExe;
        externalProc.StartInfo.Arguments = "-d";
        externalProc.Start();
        externalProc.WaitForExit();
        Console.WriteLine("DSE disabled");

        externalProc.StartInfo.FileName = devconExe;
        externalProc.StartInfo.Arguments = "restart =Display";
        externalProc.Start();
        externalProc.WaitForExit();

        externalProc.StartInfo.FileName = efidsefixExe;
        externalProc.StartInfo.Arguments = "-e";
        externalProc.Start();
        externalProc.WaitForExit();

        Console.WriteLine("DSE re-enabled");
    }

    static void msiEnable() {
        //First need to get the instance IDs of the GPUs.

        var iids = nvidiaInstanceIDs();
		
		if ((iids == null) || (iids.Count == 0)) {
			Console.WriteLine("Found no NVIDIA device instance IDs to act upon. Closing.");
			return;
		}

        RegistryKey hklm_root = Registry.LocalMachine;

        foreach (string iid in iids) {
            RegistryKey deviceKey = hklm_root.OpenSubKey(@"SYSTEM\CurrentControlSet\Enum\" + iid, true);

            if (deviceKey == null) {
                Console.WriteLine("Could not open Registry key for GPU.");
            } else {
				RegistryKey msiKey = deviceKey.CreateSubKey(@"Device Parameters\Interrupt Management\MessageSignaledInterruptProperties");
                Console.WriteLine("Setting MSISupported on key: " + msiKey.Name);
                msiKey.SetValue("MSISupported", 1, RegistryValueKind.DWord);
            }
        }
    }

    static List<string> nvidiaInstanceIDs() {

        string[] resetUtils = Directory.GetFiles(Directory.GetCurrentDirectory(), bootUtilNames[1], SearchOption.AllDirectories);
        if (resetUtils.Length < 1) {
            Console.WriteLine("Unable to find devcon.exe, cannot scan for Instance IDs to set MSI enable in registry");
            return null;
        }
        string devconExe = resetUtils[0];


        var returnable = new List<string>();

        System.Diagnostics.Process externalProc = new System.Diagnostics.Process();
        externalProc.StartInfo.FileName = devconExe;
        externalProc.StartInfo.Arguments = "listclass Display";
		externalProc.StartInfo.UseShellExecute = false;
        externalProc.StartInfo.RedirectStandardOutput = true;
        externalProc.Start();

        using (externalProc.StandardOutput) {
            string line;
            string[] splitter = {": "};
            while ((line = externalProc.StandardOutput.ReadLine()) != null) {
                string[] iidAndName = line.Split(splitter, StringSplitOptions.None);
                if (iidAndName.Length == 2) {
                    if (iidAndName[1].Contains("NVIDIA")) {
                        Console.WriteLine("Found NVIDIA GPU with Instance ID, name: " + iidAndName[0] + ", " + iidAndName[1]);
                        returnable.Add(iidAndName[0]);
                    }
                }

            }
        }

        externalProc.WaitForExit();
        
        if (returnable.Count < 1) {
            Console.WriteLine("Found no NVIDIA GPUs on which to set MSI.");
        } else if (returnable.Count < 2) {
            Console.WriteLine("Only found one GPU. If you're running SLI this may be in error; please manually set MSI.");
        }

        return returnable;
    }

}
