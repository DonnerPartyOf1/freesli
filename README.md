# FreeSLI NVIDIA driver patcher


FreeSLI is a command-line application that scans for the latest installed NVIDIA driver, patches it to enable SLI on incompatible motherboards, 
and leverages tools provided by the Windows SDK and some community assistance to install the driver in-place while booted into Safe Mode w/ Networking.

**I take no responsibility for any damage that occurs to your machine or data as a result of using this software.**

---

# Use

1. Download the .zip file containing the program executable and folder of tools. Unzip wherever you please.
2. Install your driver. DDU beforehand is recommended but not necessary; FreeSLI will detect the latest driver version by file modification date.
3. Reboot into Safe Mode w/ Networking. Networking is **required** to timestamp the new driver file.
4. In an elevated shell (cmd and Powershell are both fine) navigate to the folder where you unzipped the distribution package.
5. Execute FreeSLI.exe. If all goes well, it will drop a patched driver file into the executable's directory, then ask you if you'd like to install now.
6. If the install is successful, FreeSLI will prompt you to enable Message-Signaled Interrupts. Read below for details and **have a backup if you have never enabled this before.**
8. Reboot your machine and the driver will be in place. Make sure you have a solution in place to enable test signing mode or otherwise activate the hacked driver.

# Advanced Usage

| Argument | Effect | Example |
| ------ | ------ | ------ |
| filename | Invoke patcher on specific file | FreeSLI.exe nvlddmkm.sys |
| --reset/-R | Disables DSE, resets display devices, re-enables DSE | FreeSLI.exe --reset |
| --msi/-M | Enable MSI on NVIDIA GPUs | FreeSLI.exe --msi |

---

# Features
  - Detect installed NVIDIA driver files and patch for SLI compatibility
  - Sign patched driver and install new driver into place
  - Prompts user to enable Message-Signaled Interrupts after install
  - If running [EfiGuard](https://github.com/Mattiwatti/EfiGuard), can execute DSE disable, device reset, DSE enable

# SLI patch

See .txt file in repository for an explanation of the patch. Special thanks to Pretentious @ TechPowerUp forums for his work on function tracing to find 
the initial byte offsets in this generation of drivers.

# Tooling

Distributed with the package are: Signtool, makecert, CertMgr, and DevCon, which are distributed with Windows or the Windows SDK and
fall under the Microsoft Public License. See [here](https://github.com/microsoft/Windows-driver-samples) if you really have a desire to build these from source.

Also included are ChecksumFix, initially provided by the [DifferentSLIAuto](https://github.com/EmberVulpix/DifferentSLIAuto) project under no specific license, and EfiDSEFix, provided by [EfiGuard](https://github.com/Mattiwatti/EfiGuard) under GPLv3.

FreeSLI calls into these tools as external processes so while they are distributed with the package, FreeSLI code is not a derivative work of any.

# Message Signaled Interrupts

[Message Signaled Interrupts](https://www.intel.com/content/dam/www/public/us/en/documents/white-papers/msg-signaled-interrupts-paper.pdf) is a chipset feature that can minimize device interrupt latency and reduce traffic on the PCIe lanes, the latter of which is particularly useful for SLI setups with constrained lane counts.

Unfortunately there exists a subset of motherboard implementations and peripheral devices that not only cause the device to stop working, but fail so terrifically that Windows simply BSODs on boot, requiring a snapshot recovery. Thankfully in 2020 this is exceedingly rare; the most common case for this was somewhat ironically NVIDIA's own nForce chipsets which went EOL in the Core 2 Duo era. So this should be safe for nearly everyone. 

Note that this feature doesn't enable MSI on the other graphics card devices, e.g. HDMI/DP audio, USB-C controllers. For more fine-grained control I recommend [MSI Utility.](https://github.com/CHEF-KOCH/MSI-utility/releases)

# DSE

DSE, or Driver Signature Enforcement, prevents Windows 10 from loading the patched driver on boot. There are a few different ways of getting around this. You can globally enable test signing mode, but most online game anticheats will prevent loading with this enabled. I recommend using [https://github.com/Mattiwatti/EfiGuard](EfiGuard), the userspace component of which is distributed with FreeSLI for a quick device reset on boot.

# License

GPLv3

